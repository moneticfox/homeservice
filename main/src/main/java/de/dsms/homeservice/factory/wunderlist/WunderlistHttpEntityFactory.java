package de.dsms.homeservice.factory.wunderlist;

import de.dsms.homeservice.config.wunderlist.WunderlistConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class WunderlistHttpEntityFactory {

    private final WunderlistConfig wunderlistConfig;

    public HttpEntity<String> createGetEntity() {
        return createPostEntity("parameters");
    }

    public <T> HttpEntity<T> createPostEntity(T request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("X-Access-Token", wunderlistConfig.getAccessToken());
        headers.set("X-Client-ID", wunderlistConfig.getClientId());
        return new HttpEntity<>(request, headers);
    }
}
