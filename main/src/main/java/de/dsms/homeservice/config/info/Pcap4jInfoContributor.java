package de.dsms.homeservice.config.info;

import de.dsms.homeservice.service.SmartListener;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collections;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class Pcap4jInfoContributor implements InfoContributor {

    private final SmartListener pcap4jListener;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("Pcap4jListener", Collections.singletonMap("status", pcap4jListener.getStatus()));
    }
}
