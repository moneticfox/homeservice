package de.dsms.homeservice.config;

import de.dsms.homeservice.config.pcap.Pcap4jConfig;
import de.dsms.homeservice.config.smart.SmartConfig;
import de.dsms.homeservice.config.wunderlist.WunderlistConfig;
import de.dsms.homeservice.config.wunderlist.WunderlistEndpointConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.inject.Inject;

@Configuration
@EnableConfigurationProperties({WunderlistConfig.class, WunderlistEndpointConfig.class,
        SmartConfig.class, Pcap4jConfig.class})
public class ApplicationConfig extends WebMvcConfigurerAdapter {

    @Inject
    private LoggerInterceptor loggerInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loggerInterceptor);
    }
}
