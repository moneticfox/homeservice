package de.dsms.homeservice.config.info;

import de.dsms.homeservice.service.smart.SmartRetryQueue;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collections;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SmartRetryQueueInfoContributor implements InfoContributor {

    private final SmartRetryQueue smartRetryQueue;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("SmartRetryQueue",
                Collections.singletonMap("entries", smartRetryQueue.getQueue()
                        .stream().collect(Collectors.toList())));
    }
}
