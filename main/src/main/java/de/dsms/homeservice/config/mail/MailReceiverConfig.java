package de.dsms.homeservice.config.mail;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "homeservice.mail.receiver")
public class MailReceiverConfig {

    private String host;
    private String user;
    private String password;
}
