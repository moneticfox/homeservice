package de.dsms.homeservice.config.smart;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "homeservice.smart")
public class SmartConfig {

    private long shoppingListId;
    private Integer preserveLogInDays;
}
