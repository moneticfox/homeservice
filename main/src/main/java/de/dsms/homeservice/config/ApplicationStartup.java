package de.dsms.homeservice.config;

import de.dsms.homeservice.service.SmartListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private final List<SmartListener> smartListeners;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        smartListeners.forEach(SmartListener::listen);
    }
}
