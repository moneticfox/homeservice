package de.dsms.homeservice.config.pcap;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "homeservice.pcap")
public class Pcap4jConfig {

    private String ip;
}
