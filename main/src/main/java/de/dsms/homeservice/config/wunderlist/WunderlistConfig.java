package de.dsms.homeservice.config.wunderlist;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "homeservice.wunderlist")
public class WunderlistConfig {

    private String accessToken;
    private String clientId;
    private String baseUrl;
}
