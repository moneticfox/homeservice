package de.dsms.homeservice.entity;

import de.dsms.homeservice.model.smart.SmartSource;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SmartServiceResult implements Identifiable<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String serviceName = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private SmartSource source;

    private String result = "";

    private boolean error = false;

    @Setter(AccessLevel.PRIVATE)
    private Date createdAt;

    @PrePersist
    void createdAt() {
        this.createdAt = new Date();
    }

    public SmartServiceResult(String serviceName, SmartSource source, String result, boolean error) {
        this.serviceName = serviceName;
        this.source = source;
        this.result = result;
        this.error = error;
    }
}
