package de.dsms.homeservice.entity;

import de.dsms.homeservice.model.smart.SmartFunction;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ArpSource implements Identifiable<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min=17,max=17)
    private String macAddress = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private SmartFunction function;

    private String argument = "";

    @NotNull
    private String name = "";

    @Setter(AccessLevel.PRIVATE)
    private Date createdAt;

    @PrePersist
    void createdAt() {
        this.createdAt = new Date();
    }

    public ArpSource(String macAddress, SmartFunction function, String name, String argument) {
        this.macAddress = macAddress;
        this.function = function;
        this.name = name;
        this.argument = argument;
    }
}
