package de.dsms.homeservice.entity;

public interface Identifiable<ID> {
    ID getId();
}
