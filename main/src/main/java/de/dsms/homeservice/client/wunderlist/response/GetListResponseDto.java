package de.dsms.homeservice.client.wunderlist.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetListResponseDto {

    private long id;
    private String title;
}
