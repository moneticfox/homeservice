package de.dsms.homeservice.client.wunderlist;

import de.dsms.homeservice.client.util.RestTemplateUtil;
import de.dsms.homeservice.config.wunderlist.WunderlistConfig;
import de.dsms.homeservice.factory.wunderlist.WunderlistHttpEntityFactory;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
class WunderlistRestTemplate {

    private final WunderlistConfig wunderlistConfig;
    private final WunderlistHttpEntityFactory httpEntityFactory;
    private final RestTemplateUtil restTemplateUtil;

    private RestTemplate restTemplate;

    @Inject
    @Required
    private void setRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    <T> T get(String endpoint, Class<T> responseClass) {
        return get(endpoint, responseClass, StringUtils.EMPTY);
    }

    <T> T get(String endpoint, Class<T> responseClass, Pair<String, String>... queryParams) {
        return get(endpoint, responseClass, restTemplateUtil.toQuery(queryParams));
    }

    <T> T get(String endpoint, Class<T> responseClass, String query) {
        String url = createUrl(endpoint);

        if(StringUtils.isNotBlank(query)) {
            if(query.startsWith("?")) {
                url += query;
            } else {
                url += "/" + query;
            }
        }

        return restTemplate.exchange(url, HttpMethod.GET, httpEntityFactory.createGetEntity(), responseClass)
                .getBody();
    }

    <T> T post(String endpoint, Class<T> responseClass, Object request) {
        return restTemplate.exchange(createUrl(endpoint), HttpMethod.POST, httpEntityFactory.createPostEntity(request), responseClass)
                .getBody();
    }

    private String createUrl(String endpoint) {
        return wunderlistConfig.getBaseUrl() + endpoint;
    }
}
