package de.dsms.homeservice.client.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

@Service
public class RestTemplateUtil {

    public final String toQuery(Pair<String, String>... queryParams) {
        if(queryParams == null || queryParams.length == 0) {
            return StringUtils.EMPTY;
        }

        String result = "?";

        for(Pair<String, String> pair : queryParams) {
            result += String.format("%s=%s&", pair.getKey(), pair.getValue());
        }

        return result.substring(0, result.length() - 1);
    }
}
