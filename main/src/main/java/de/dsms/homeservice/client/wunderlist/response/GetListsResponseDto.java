package de.dsms.homeservice.client.wunderlist.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class GetListsResponseDto {

    private long id;
    @JsonProperty("created_at")
    private Date createdAt;
    private String title;
    @JsonProperty("list_type")
    private String listType;
    @JsonProperty("type")
    private String type;
    private int revision;
}
