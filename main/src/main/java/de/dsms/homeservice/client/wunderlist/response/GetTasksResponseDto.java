package de.dsms.homeservice.client.wunderlist.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class GetTasksResponseDto {

    private long id;
    @JsonProperty("created_at")
    private Date createdAt;
    @JsonProperty("list_id")
    private long listId;
    private String title;
}
