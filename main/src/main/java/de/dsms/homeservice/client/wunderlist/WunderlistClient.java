package de.dsms.homeservice.client.wunderlist;

import de.dsms.homeservice.client.wunderlist.request.AddTaskRequestDto;
import de.dsms.homeservice.client.wunderlist.response.AddTaskResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetListResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetListsResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetTasksResponseDto;
import de.dsms.homeservice.config.wunderlist.WunderlistEndpointConfig;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class WunderlistClient {

    private final WunderlistEndpointConfig wunderlistEndpointConfig;
    private final WunderlistRestTemplate wunderlistRestTemplate;

    public GetListResponseDto getList(long id) {
        return wunderlistRestTemplate.get(wunderlistEndpointConfig.getLists(), GetListResponseDto.class, Long.toString(id));
    }

    public GetListsResponseDto[] getLists() {
        return wunderlistRestTemplate.get(wunderlistEndpointConfig.getLists(), GetListsResponseDto[].class);
    }

    public GetTasksResponseDto[] getTasks(long listId) {
        return wunderlistRestTemplate.get(wunderlistEndpointConfig.getTasks(), GetTasksResponseDto[].class,
                Pair.of("list_id", Long.toString(listId)));
    }

    public AddTaskResponseDto addTask(AddTaskRequestDto request) {
        return wunderlistRestTemplate.post(wunderlistEndpointConfig.getTasks(), AddTaskResponseDto.class, request);
    }
}
