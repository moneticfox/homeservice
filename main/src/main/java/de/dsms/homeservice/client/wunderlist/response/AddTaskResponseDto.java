package de.dsms.homeservice.client.wunderlist.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AddTaskResponseDto {

    private long id;
    @JsonProperty("created_at")
    private Date createdAt;
    private String title;
    @JsonProperty("list_id")
    private long listId;
}
