package de.dsms.homeservice.client.wunderlist.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.dsms.homeservice.model.wunderlist.Task;
import lombok.AllArgsConstructor;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
public class AddTaskRequestDto {

    @JsonProperty("list_id")
    private long listId;
    private String title;
}
