package de.dsms.homeservice.controller;

import de.dsms.homeservice.client.wunderlist.WunderlistClient;
import de.dsms.homeservice.client.wunderlist.request.AddTaskRequestDto;
import de.dsms.homeservice.client.wunderlist.response.AddTaskResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetListsResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetTasksResponseDto;
import de.dsms.homeservice.config.smart.SmartConfig;
import de.dsms.homeservice.model.wunderlist.Task;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("api/wunderlist")
public class WunderlistController {

    @Inject
    private WunderlistClient wunderlistClient;

    @Inject
    private SmartConfig smartConfig;

    @RequestMapping(value = "/lists", method = RequestMethod.GET)
    public GetListsResponseDto[] getLists() {
        return wunderlistClient.getLists();
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public GetTasksResponseDto[] getTask(@RequestParam("listId") long listId) {
        return wunderlistClient.getTasks(listId);
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public AddTaskResponseDto addTask(@RequestBody AddTaskRequestDto request) {
        return wunderlistClient.addTask(request);
    }

    @RequestMapping(value = "/tasks/shopping", method = RequestMethod.POST)
    public AddTaskResponseDto addShoppingListTask(@RequestBody Task task) {
        return wunderlistClient.addTask(new AddTaskRequestDto(smartConfig.getShoppingListId(), task.getTitle()));
    }
}