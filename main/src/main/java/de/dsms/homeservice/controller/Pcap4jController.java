package de.dsms.homeservice.controller;

import de.dsms.homeservice.entity.ArpSource;
import de.dsms.homeservice.repository.ArpSourceRepository;
import de.dsms.homeservice.service.SmartListener;
import lombok.extern.slf4j.Slf4j;
import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapNativeException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("api/pcap")
public class Pcap4jController {

    @Inject
    private SmartListener pcap4jListener;

    @Inject
    private ArpSourceRepository arpSourceRepository;

    @RequestMapping(value = "/start", method = RequestMethod.PUT)
    public void start() throws InterruptedException, PcapNativeException, NotOpenException, IOException {
        pcap4jListener.listen();
    }

    @RequestMapping(value = "/stop", method = RequestMethod.PUT)
    public void stop() throws NotOpenException {
        pcap4jListener.stopListening();
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status() {
        return pcap4jListener.getStatus();
    }

    @RequestMapping(value = "/sources", method = RequestMethod.GET)
    public Iterable<ArpSource> sources() {
        return arpSourceRepository.findAll();
    }

    @RequestMapping(value = "/sources", method = RequestMethod.POST)
    public ArpSource addSource(@RequestBody ArpSource arpSource) {
        return arpSourceRepository.save(arpSource);
    }
}
