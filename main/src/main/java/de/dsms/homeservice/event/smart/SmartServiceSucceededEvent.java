package de.dsms.homeservice.event.smart;

import de.dsms.homeservice.model.smart.SmartRequest;
import de.dsms.homeservice.model.smart.SmartResponse;
import lombok.Data;

@Data(staticConstructor = "of")
public class SmartServiceSucceededEvent {

    private final String serviceName;
    private final SmartRequest request;
    private final SmartResponse response;
}
