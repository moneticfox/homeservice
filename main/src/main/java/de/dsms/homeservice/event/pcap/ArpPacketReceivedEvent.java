package de.dsms.homeservice.event.pcap;

import de.dsms.homeservice.model.smart.SmartFunction;
import lombok.Data;

@Data(staticConstructor = "of")
public class ArpPacketReceivedEvent {

    private final SmartFunction function;
    private final String argument;
}
