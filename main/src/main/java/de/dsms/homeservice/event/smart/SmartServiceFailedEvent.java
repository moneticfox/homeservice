package de.dsms.homeservice.event.smart;

import de.dsms.homeservice.model.Procedure;
import de.dsms.homeservice.model.smart.SmartRequest;
import lombok.Data;

@Data(staticConstructor = "of")
public class SmartServiceFailedEvent {

    private final String serviceName;
    private final SmartRequest request;
    private final String error;
    private final Procedure retryFunction;
}
