package de.dsms.homeservice.model.smart;

import lombok.Data;

@Data(staticConstructor = "of")
public class ShoppingListRequest implements SmartRequest {

    private final SmartSource source;
    private final String title;
}
