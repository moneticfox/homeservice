package de.dsms.homeservice.model;

public interface Procedure {
    void invoke();
}
