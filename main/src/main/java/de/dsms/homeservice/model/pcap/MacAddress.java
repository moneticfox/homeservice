package de.dsms.homeservice.model.pcap;

import lombok.Data;

@Data(staticConstructor = "of")
public class MacAddress {

    private final String address;

    public boolean equals(MacAddress macAddress) {
        return equals(macAddress.getAddress());
    }

    public boolean equals(String macAddress) {
        return this.address.equalsIgnoreCase(macAddress)
                || equalsAfterNormalizing(macAddress);
    }

    private boolean equalsAfterNormalizing(String address) {
        final String normalizedThisAddress = this.address.replace("-", "")
                .replace(":", "")
                .replace(".", "");
        final String normalizedOtherAddress = address.replace("-", "")
                .replace(":", "")
                .replace(".", "");

        return normalizedThisAddress.equalsIgnoreCase(normalizedOtherAddress);
    }
}
