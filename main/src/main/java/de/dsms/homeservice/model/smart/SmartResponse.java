package de.dsms.homeservice.model.smart;

public interface SmartResponse {

    String getResult();
}
