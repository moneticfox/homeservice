package de.dsms.homeservice.model.wunderlist;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data(staticConstructor = "of")
public class Task {

    private final String title;
    private List<Task> subtasks;

    public boolean addSubtask(Task task) {
        if(subtasks == null) {
            subtasks = new ArrayList<>();
        }
        return subtasks.add(task);
    }
}
