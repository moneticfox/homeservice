package de.dsms.homeservice.model.smart;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.dsms.homeservice.model.Procedure;
import lombok.Data;

import java.time.Instant;

@Data(staticConstructor = "of")
public class RetryableFunction {

    @JsonIgnore
    private final Procedure procedure;
    private Instant queueingTime = Instant.now();
    private Instant lastRetry = null;
    private int triedTimes = 0;
}
