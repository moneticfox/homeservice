package de.dsms.homeservice.model.smart;

import lombok.Data;

@Data(staticConstructor = "of")
public class ShoppingListResponse implements SmartResponse {

    private final String result;
}
