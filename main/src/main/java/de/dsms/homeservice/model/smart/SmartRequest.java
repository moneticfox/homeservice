package de.dsms.homeservice.model.smart;

public interface SmartRequest {

    SmartSource getSource();
}
