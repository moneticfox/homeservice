package de.dsms.homeservice.repository;

import de.dsms.homeservice.entity.ArpSource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArpSourceRepository extends JpaRepository<ArpSource, Long> {

    List<ArpSource> findByMacAddressIgnoringCaseOrderByCreatedAtDesc(String macAddress);

    List<ArpSource> findByMacAddressContainingOrNameContainingAllIgnoringCaseOrderByCreatedAtDesc(String macAddress, String name);
}
