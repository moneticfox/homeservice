package de.dsms.homeservice.repository;

import de.dsms.homeservice.entity.SmartServiceResult;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SmartServiceResultRepository extends JpaRepository<SmartServiceResult, Long> {

    List<SmartServiceResult> findByCreatedAtLessThan(Date createdAt);

    List<SmartServiceResult> findByResultContainingOrServiceNameContainingAllIgnoringCaseOrderByCreatedAtDesc(String source, String serviceName);
}
