package de.dsms.homeservice.service.util;

import java.lang.reflect.Field;

public class Objects {

    public static <T> T firstNonNull(T ...items) {
        for(T i : items) if(i != null) return i;
        return null;
    }

    private static <T> T merge(T specific, T fallback) throws IllegalAccessException, InstantiationException {
        if (specific == null && fallback == null) {
            return null;
        }

        final Class<?> clazz = specific != null ? specific.getClass() : fallback.getClass();
        final T result = (T)clazz.newInstance();

        for(Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            final Object specificValue = specific != null ? field.get(specific) : null;
            final Object fallbackValue = fallback != null ? field.get(fallback) : null;
            final Object resultValue;

            if (!field.getType().isPrimitive() && !field.getType().equals(String.class)) {
                resultValue = merge(specificValue, fallbackValue);
            } else {
                resultValue = specificValue != null ? specificValue : fallbackValue;
            }

            field.set(result, resultValue);
        }

        return result;
    }
}
