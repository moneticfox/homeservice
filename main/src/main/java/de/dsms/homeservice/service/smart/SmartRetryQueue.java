package de.dsms.homeservice.service.smart;

import de.dsms.homeservice.model.smart.RetryableFunction;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
@Service
public class SmartRetryQueue {

    @Getter
    private final Queue<RetryableFunction> queue = new ConcurrentLinkedQueue<>();

    boolean enqueue(RetryableFunction entry) {
        log.info("Retry einer Smart-Funktion wird eingereiht {}", entry);
        return queue.add(entry);
    }

    @Scheduled(fixedDelay = 10000)
    private void process() {
        final RetryableFunction entry = queue.peek();

        if(entry != null) {
            log.info("Retry wird gestartet für {}", entry);
            try {
                entry.getProcedure().invoke();
                queue.remove(entry);
            } catch (Exception e) {
                log.error("Retry fehlgeschlagen für {} ", entry);
                entry.setTriedTimes(entry.getTriedTimes() + 1);
                entry.setLastRetry(Instant.now());
            }
        }
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder("");

        if(!queue.isEmpty()) {
            queue.forEach(e -> result.append(String.format("%s\n", e.toString())));
        }
        return result.toString();
    }
}
