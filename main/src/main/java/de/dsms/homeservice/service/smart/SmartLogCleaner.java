package de.dsms.homeservice.service.smart;

import de.dsms.homeservice.config.smart.SmartConfig;
import de.dsms.homeservice.entity.SmartServiceResult;
import de.dsms.homeservice.repository.SmartServiceResultRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SmartLogCleaner {

    private final SmartConfig smartConfig;
    private final SmartServiceResultRepository repository;

    // Every morning at 10 am
    @Scheduled(cron = "0 */20 10 * * *")
    private void clean() {
        // delete logentries being too old
        int daysToLog = smartConfig.getPreserveLogInDays() != null ?
                smartConfig.getPreserveLogInDays() : 30;
        final Date comparable = Date.from(Instant.now().minus(daysToLog, ChronoUnit.DAYS));
        final List<SmartServiceResult> results = repository.findByCreatedAtLessThan(comparable);

        log.info("SmartLogCleaner löscht die folgenden SmartServiceResults {}", results);

        repository.deleteInBatch(results);
    }
}
