package de.dsms.homeservice.service;

/**
 * Created by Dennis on 01.10.2016.
 */
public interface SmartListener {

    void listen();

    void stopListening();

    String getStatus();
}
