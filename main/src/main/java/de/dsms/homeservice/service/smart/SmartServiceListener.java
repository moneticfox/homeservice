package de.dsms.homeservice.service.smart;

import de.dsms.homeservice.entity.SmartServiceResult;
import de.dsms.homeservice.event.smart.SmartServiceFailedEvent;
import de.dsms.homeservice.event.smart.SmartServiceSucceededEvent;
import de.dsms.homeservice.model.smart.RetryableFunction;
import de.dsms.homeservice.repository.SmartServiceResultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SmartServiceListener {

    private final SmartServiceResultRepository repository;
    private final SmartRetryQueue retryQueue;

    @Async
    @EventListener
    public void handleSmartServiceSucceeded(SmartServiceSucceededEvent event) {
        final SmartServiceResult persistable = new SmartServiceResult(event.getServiceName(),
                event.getRequest().getSource(), event.getResponse().getResult(), false);

        repository.save(persistable);
    }

    @Async
    @EventListener
    public void handleSmartServiceFailed(SmartServiceFailedEvent event) {
        final SmartServiceResult persistable = new SmartServiceResult(event.getServiceName(),
                event.getRequest().getSource(), event.getError(), true);

        repository.save(persistable);

        if (event.getRetryFunction() != null) {
            retryQueue.enqueue(RetryableFunction.of(event.getRetryFunction()));
        }
    }
}
