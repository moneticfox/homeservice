package de.dsms.homeservice.service.smart.function;

import de.dsms.homeservice.event.smart.SmartServiceFailedEvent;
import de.dsms.homeservice.event.smart.SmartServiceSucceededEvent;
import de.dsms.homeservice.model.smart.SmartRequest;
import de.dsms.homeservice.model.smart.SmartResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public abstract class SmartService<T extends SmartRequest, R extends SmartResponse> {

    protected final ApplicationEventPublisher eventPublisher;

    public final void process(T request) {
        try {
            log.info("SmartService {} wird mit folgender Anfrage ausgeführt: {}", getServiceName(), request);
            final Optional<R> result = doIt(request);

            if(result.isPresent()) {
                log.info("SmartService {} erfolgreich ausgeführt: {}", getServiceName(), result.get());
                eventPublisher.publishEvent(SmartServiceSucceededEvent.of(getServiceName(), request, result.get()));
            } else {
                log.warn("Bei Ausführung eines SmartServices konnte kein Ergebnis ermittelt werden.");
            }
        } catch(Exception e) {
            log.error("Bei Ausführung eines SmartServices ist ein Fehler aufgetreten", e);
            publishFailure(request, e.toString(), false);
        }
    }

    protected abstract Optional<R> doIt(T request);

    protected Optional<R> publishFailure(T request, String error, boolean shouldRetry) {
        eventPublisher.publishEvent(SmartServiceFailedEvent.of(getServiceName(), request, error,
                shouldRetry ? () -> process(request) : null));
        return Optional.empty();
    }

    protected String getServiceName() {
        return this.getClass().getName();
    }
}
