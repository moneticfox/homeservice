package de.dsms.homeservice.service.smart.function;

import de.dsms.homeservice.client.wunderlist.response.AddTaskResponseDto;
import de.dsms.homeservice.config.smart.SmartConfig;
import de.dsms.homeservice.model.smart.ShoppingListRequest;
import de.dsms.homeservice.model.smart.ShoppingListResponse;
import de.dsms.homeservice.service.wunderlist.WunderlistService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ShoppingListService extends SmartService<ShoppingListRequest, ShoppingListResponse> {

    private final SmartConfig smartConfig;
    private final WunderlistService wunderlistService;

    public ShoppingListService(ApplicationEventPublisher eventPublisher,
                               SmartConfig smartConfig,
                               WunderlistService wunderlistService) {
        super(eventPublisher);
        this.smartConfig = smartConfig;
        this.wunderlistService = wunderlistService;
    }

    @Override
    protected Optional<ShoppingListResponse> doIt(ShoppingListRequest request) {
        if(StringUtils.isBlank(request.getTitle())) {
            String error = String.format("ShoppingListRequest enthält keinen Task-Title: %s", request);
            log.error(error);
            return publishFailure(request, error, false);
        }

        final Optional<AddTaskResponseDto> taskResponseDto =
                    wunderlistService.addUniqueTask(smartConfig.getShoppingListId(), request.getTitle());

        if(!taskResponseDto.isPresent()) {
            return publishFailure(request, "Bei der Anlage eines Tasks ist ein Fehler aufgetreten.", true);
        }

        return Optional.of(ShoppingListResponse.of(Long.toString(taskResponseDto.get().getId())));
    }
}
