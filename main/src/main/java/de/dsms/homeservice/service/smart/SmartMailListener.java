package de.dsms.homeservice.service.smart;

import de.dsms.homeservice.service.mail.MailReceiver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SmartMailListener {

    private final MailReceiver mailReceiver;

    public void listen() {
        mailReceiver.receive(getMessageCountListener());
    }

    private MessageCountListener getMessageCountListener() {
        return new MessageCountListener() {
            @Override
            public void messagesAdded(MessageCountEvent messageCountEvent) {
                log.info(messageCountEvent.toString());
            }

            @Override
            public void messagesRemoved(MessageCountEvent messageCountEvent) {
                log.info(messageCountEvent.toString());
            }
        };
    }
}
