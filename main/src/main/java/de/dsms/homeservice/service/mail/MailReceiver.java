package de.dsms.homeservice.service.mail;

import de.dsms.homeservice.config.mail.MailReceiverConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.event.MessageCountListener;
import java.util.Properties;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class MailReceiver {

    private final MailReceiverConfig mailReceiverConfig;

    public void receive(final MessageCountListener messageCountListener) {
        final Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");
        final Session session = Session.getDefaultInstance(props, null);

        try {
            final Folder inbox = getInbox(session);
            inbox.addMessageCountListener(messageCountListener);
        } catch (MessagingException e) {
            log.error(e.toString());
        }
    }

    private Folder getInbox(Session session) throws MessagingException {
        final Store store = session.getStore("imaps");
        store.connect(mailReceiverConfig.getHost(), mailReceiverConfig.getUser(), mailReceiverConfig.getPassword());

        return store.getFolder("INBOX");
    }
}
