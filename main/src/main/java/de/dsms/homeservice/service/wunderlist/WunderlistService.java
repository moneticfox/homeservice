package de.dsms.homeservice.service.wunderlist;

import de.dsms.homeservice.client.wunderlist.WunderlistClient;
import de.dsms.homeservice.client.wunderlist.request.AddTaskRequestDto;
import de.dsms.homeservice.client.wunderlist.response.AddTaskResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetListResponseDto;
import de.dsms.homeservice.client.wunderlist.response.GetTasksResponseDto;
import de.dsms.homeservice.model.wunderlist.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;

import javax.inject.Inject;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class WunderlistService {

    private final WunderlistClient wunderlistClient;

    public Optional<AddTaskResponseDto> addUniqueTask(final long listId, String title) {
        return addTask(listId, title, true);
    }

    public AddTaskResponseDto addTask(final long listId, String title) {
        return addTask(listId, title, false).get();
    }

    private Optional<AddTaskResponseDto> addTask(final long listId, final String title, final boolean isUnique) {
        log.info("Task {} soll zur Liste {} hinzugefügt werden.", title, listId);

        try {
            GetListResponseDto listResponse = wunderlistClient.getList(listId);

            if (listResponse == null || StringUtils.isBlank(listResponse.getTitle())) {
                log.error("Liste {} existiert nicht: {}", listId, listResponse);
                throw new IllegalArgumentException(Long.toString(listId));
            }

            log.info("Liste {} gefunden: {}.", listResponse.getId(), listResponse.getTitle());

            final GetTasksResponseDto[] tasksResponses = wunderlistClient.getTasks(listResponse.getId());

            if (isUnique && tasksResponses != null) {
                final Optional<GetTasksResponseDto> existingTask = getMaybe(tasksResponses, title);

                if (existingTask.isPresent()) {
                    log.info("Task {} existiert bereits.", title);
                    return Optional.of(createResponseOfExisting(existingTask.get()));
                }
            }

            log.info("Task {} existiert noch nicht und wird angelegt", title);

            final AddTaskResponseDto response = wunderlistClient.addTask(new AddTaskRequestDto(listId, title));
            log.info("Task {} angelegt", response.getTitle());

            return Optional.of(response);
        } catch(RestClientResponseException re) {
            log.error("Anfrage bei Wunderlist fehlgeschlagen", re);
            return Optional.empty();
        }
    }

    private AddTaskResponseDto createResponseOfExisting(GetTasksResponseDto getTasksResponseDto) {
        return new AddTaskResponseDto() {{
            setId(getTasksResponseDto.getId());
            setCreatedAt(getTasksResponseDto.getCreatedAt());
            setListId(getTasksResponseDto.getListId());
            setTitle(getTasksResponseDto.getTitle());
        }};
    }

    private Optional<GetTasksResponseDto> getMaybe(GetTasksResponseDto[] tasksResponses, String title) {
        for(GetTasksResponseDto tasksResponse : tasksResponses) {
            if(tasksResponse.getTitle().equalsIgnoreCase(title)) {
                return Optional.of(tasksResponse);
            }
        }
        return Optional.empty();
    }
}
