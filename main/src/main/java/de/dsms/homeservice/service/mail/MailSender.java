package de.dsms.homeservice.service.mail;

import de.dsms.homeservice.config.mail.MailSenderConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class MailSender {

    private final JavaMailSender javaMailSender;
    private final MailSenderConfig mailSenderConfig;

    public void send(String subject, String text) {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
            messageHelper.setFrom(mailSenderConfig.getFrom());
            messageHelper.setTo(mailSenderConfig.getTo());
            messageHelper.setSubject(subject);
            messageHelper.setText(text);
        } catch (MessagingException me) {
            log.error("Fehler beim Senden einer E-Mail", me);
        }
        javaMailSender.send(message);
    }
}
