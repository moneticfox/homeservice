package de.dsms.homeservice.service.pcap;

import de.dsms.homeservice.config.pcap.Pcap4jConfig;
import de.dsms.homeservice.entity.ArpSource;
import de.dsms.homeservice.event.pcap.ArpPacketReceivedEvent;
import de.dsms.homeservice.model.pcap.MacAddress;
import de.dsms.homeservice.repository.ArpSourceRepository;
import de.dsms.homeservice.service.SmartListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.core.*;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.ArpPacket.ArpHeader;
import org.pcap4j.packet.Packet;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.net.InetAddress;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class Pcap4jListener implements SmartListener {

    private final Pcap4jConfig pcap4jConfig;
    private final ApplicationEventPublisher eventPublisher;
    private final ArpSourceRepository arpSourceRepository;

    private PcapHandle pcapHandle;
    private Map<String, Instant> arpLog = new ConcurrentHashMap<>();

    @Async
    public void listen() {
        if (StringUtils.isBlank(pcap4jConfig.getIp())) {
            log.error("homeservice.ip ist nicht konfiguriert");
            return;
        }

        if (pcapHandle != null) {
            log.error("Es ist bereits in PcapHandle geöffnet: {}", pcapHandle);
            return;
        }

        try {
            InetAddress addr = InetAddress.getByName(pcap4jConfig.getIp());
            log.info("Konfigurierte IP-Adresse: " + addr);

            PcapNetworkInterface networkInterface = Pcaps.getDevByAddress(addr);

            if(networkInterface == null) {
                log.error("Zur konfigurierten IP-Adresse konnte kein NetworkInterface ermittelt werden.");
                return;
            }

            log.info("NetworkInterface: " + networkInterface.toString());

            this.pcapHandle = networkInterface.openLive(65536, PromiscuousMode.PROMISCUOUS, 10);
            this.pcapHandle.setFilter("arp", BpfCompileMode.OPTIMIZE);
            PacketListener packetListener = this::receiveMessage;

            this.pcapHandle.loop(-1, packetListener);
        } catch (InterruptedException ee) {
            log.info("PcapHandle-Loop unterbrochen.");
        } catch (IOException | PcapNativeException | NotOpenException e) {
            log.error("Fehler beim Lauschen auf ARP-Requests", e);
        }
    }

    public void stopListening() {
        if(pcapHandle == null) {
            log.error("PcapHandle-Loop kann nicht beendet werden, da noch keines gestartet wurde.");
            return;
        }

        try {
            this.pcapHandle.breakLoop();
        } catch (NotOpenException e) {
            log.warn("PcapHandle-Loop konnte nicht unterbrochen werden, da er nicht geöffnet war.", e);
        } finally {
            this.pcapHandle.close();
            this.pcapHandle = null;
        }
        log.info("PcapHandle-Loop beendet.");
    }

    public String getStatus() {
        if(pcapHandle != null) {
            return pcapHandle.toString();
        }
        return StringUtils.EMPTY;
    }

    private void receiveMessage(Packet packet) {
        if(packet.getPayload().getHeader() instanceof ArpHeader) {
            final ArpHeader arpHeader = (ArpHeader)packet.getPayload().getHeader();
            final MacAddress macAddress = MacAddress.of(arpHeader.getSrcHardwareAddr().toString());

            log.debug("ARP-Request eingegangen: {}", macAddress.toString());
            final Iterable<ArpSource> arpSources = arpSourceRepository
                    .findByMacAddressIgnoringCaseOrderByCreatedAtDesc(macAddress.getAddress());

            arpSources.forEach(s -> {
                // configure delay, since there will be multiple ARP-requests for a single connection attempt
                Instant lastTime = arpLog.getOrDefault(s.getMacAddress(), null);
                if(lastTime != null) {
                    // three seconds should be sufficient
                    if(lastTime.plusMillis(3000).isAfter(Instant.now())){
                        return;
                    }
                    arpLog.remove(s.getMacAddress());
                }
                log.info("Paket von bekannter Quelle empfangen: {}", s.getMacAddress());
                eventPublisher.publishEvent(ArpPacketReceivedEvent.of(s.getFunction(), s.getArgument()));
                arpLog.put(s.getMacAddress(), Instant.now());
            });
        }
    }
}
