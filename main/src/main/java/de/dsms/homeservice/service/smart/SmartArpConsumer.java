package de.dsms.homeservice.service.smart;

import de.dsms.homeservice.event.pcap.ArpPacketReceivedEvent;
import de.dsms.homeservice.model.smart.ShoppingListRequest;
import de.dsms.homeservice.model.smart.SmartSource;
import de.dsms.homeservice.service.smart.function.ShoppingListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SmartArpConsumer {

    private final ShoppingListService shoppingListService;

    @Async
    @EventListener
    public void handleArpPacketReceived(ArpPacketReceivedEvent event) {
        if(event == null || event.getFunction() == null) {
            log.error("ArpPacketReceivedEvent invalide: {}", event);
            return;
        }

        switch(event.getFunction()) {
            case SHOPPINGLIST:
                ShoppingListRequest request = ShoppingListRequest.of(SmartSource.ARP, event.getArgument());
                shoppingListService.process(request);
                break;
            default:
                log.error("Smart-Funktion unbekannt: {}", event.getFunction());
        }
    }
}
