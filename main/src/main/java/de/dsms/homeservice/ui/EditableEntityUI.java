package de.dsms.homeservice.ui;

import com.vaadin.event.SelectionEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import de.dsms.homeservice.entity.Identifiable;
import de.dsms.homeservice.ui.component.EntityEditor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

@Slf4j
public abstract class EditableEntityUI<T extends Identifiable<ID>, ID extends Serializable> extends EntityUI<T, ID> {

    protected final EntityEditor<T, ID> editor;

    protected Button addNew;

    public EditableEntityUI(JpaRepository<T, ID> repository, EntityEditor<T, ID> editor, Class<T> entityClass) {
        super(repository, entityClass);
        this.editor = editor;
    }

    @Override
    protected void init(VaadinRequest request) {
        super.init(request);

        grid.addSelectionListener(this::gridSelectionListener);

        addNew = new Button("Neuer Eintrag", FontAwesome.PLUS);
        addNew.addClickListener(this::addNewClickListener);

        editor.setChangeHandler(this::changeHandler);

        HorizontalLayout actions = new HorizontalLayout(filter, addNew);
        actions.setSpacing(true);

        VerticalLayout gridLayout = new VerticalLayout(actions, grid);
        gridLayout.setSpacing(true);
        gridLayout.setWidth(100, Unit.PERCENTAGE);

        HorizontalLayout mainLayout = new HorizontalLayout(gridLayout, editor);
        mainLayout.setWidth(100, Unit.PERCENTAGE);
        mainLayout.setExpandRatio(gridLayout, 0.7f);
        mainLayout.setExpandRatio(editor, 0.3f);
        mainLayout.setMargin(true);
        mainLayout.setSpacing(true);

        setContent(mainLayout);
    }

    protected void changeHandler() {
        editor.setVisible(false);
        listEntities(filter.getValue());
    }

    protected void addNewClickListener(Button.ClickEvent clickEvent) {
        try {
            editor.editEntity(entityClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("Für die anzulegende Entität existiert kein NoArgsConstructor.");
        }
    }

    protected void gridSelectionListener(SelectionEvent e) {
        if (e.getSelected().isEmpty()) {
            editor.setVisible(false);
        } else {
            editor.editEntity((T) grid.getSelectedRow());
        }
    }
}
