package de.dsms.homeservice.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
@Theme("valo")
public class StartUI extends UI {

    private Panel panelUI = new Panel("Administration");
    private Link arpSources = new Link("ARP-Quellen", new ExternalResource("pcap/sources"));
    private Link smartServiceResults = new Link("SmartService-Ergebnisse", new ExternalResource("smart/results"));

    private Panel panelEndpoints = new Panel("Endpunkte");
    private Link info = new Link("Info", new ExternalResource("info"));
    private Link logfile = new Link("Logfile", new ExternalResource("logfile"));
    private Link health = new Link("Health", new ExternalResource("health"));
    private Link mappings = new Link("Mappings", new ExternalResource("mappings"));
    private Link trace = new Link("Trace", new ExternalResource("trace"));

    @Override
    protected void init(VaadinRequest request) {
        panelUI.setStyleName(ValoTheme.PANEL_SCROLL_INDICATOR);
        arpSources.setStyleName(ValoTheme.LINK_LARGE);
        smartServiceResults.setStyleName(ValoTheme.LINK_LARGE);
        VerticalLayout panelUIVerticalLayout = new VerticalLayout(arpSources, smartServiceResults);
        panelUIVerticalLayout.setMargin(true);
        panelUIVerticalLayout.setSpacing(true);
        panelUI.setContent(panelUIVerticalLayout);

        panelEndpoints.setStyleName(ValoTheme.PANEL_SCROLL_INDICATOR);
        info.setStyleName(ValoTheme.LINK_LARGE);
        logfile.setStyleName(ValoTheme.LINK_LARGE);
        health.setStyleName(ValoTheme.LINK_LARGE);
        mappings.setStyleName(ValoTheme.LINK_LARGE);
        trace.setStyleName(ValoTheme.LINK_LARGE);
        VerticalLayout panelEndpointsVerticalLayout = new VerticalLayout(info, logfile, health, mappings, trace);
        panelEndpointsVerticalLayout.setMargin(true);
        panelEndpointsVerticalLayout.setSpacing(true);
        panelEndpoints.setContent(panelEndpointsVerticalLayout);

        VerticalLayout mainLayout = new VerticalLayout(panelUI, panelEndpoints);
        mainLayout.setMargin(true);
        mainLayout.setSpacing(true);
        setContent(mainLayout);
    }
}
