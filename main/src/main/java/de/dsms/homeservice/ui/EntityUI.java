package de.dsms.homeservice.ui;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import de.dsms.homeservice.entity.Identifiable;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@RequiredArgsConstructor(onConstructor = @__(@Inject))
public abstract class EntityUI<T extends Identifiable<ID>, ID extends Serializable> extends UI {

    protected final JpaRepository<T, ID> repository;
    protected final Class<T> entityClass;

    protected Grid grid = new Grid();
    protected TextField filter;

    @Override
    protected void init(VaadinRequest request) {
        grid.setWidth(100, Unit.PERCENTAGE);

        filter = new TextField();
        filter.setInputPrompt("Suche");
        filter.addTextChangeListener(e -> listEntities(e.getText()));

        VerticalLayout mainLayout = new VerticalLayout(filter, grid);
        mainLayout.setSpacing(true);
        mainLayout.setMargin(true);

        setContent(mainLayout);

        listEntities();
    }

    protected void listEntities() {
        listEntities(null);
    }

    protected void listEntities(String filter) {
        List<T> items;

        if(StringUtils.isBlank(filter)) {
            items = repository.findAll(new Sort(Sort.Direction.DESC, "createdAt"));
        } else {
            items = findEntityByFilter(filter);
        }

        grid.setContainerDataSource(new BeanItemContainer<>(entityClass, items));
    }

    protected abstract List<T> findEntityByFilter(String filter);
}
