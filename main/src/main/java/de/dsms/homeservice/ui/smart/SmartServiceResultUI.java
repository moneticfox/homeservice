package de.dsms.homeservice.ui.smart;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import de.dsms.homeservice.entity.SmartServiceResult;
import de.dsms.homeservice.repository.SmartServiceResultRepository;
import de.dsms.homeservice.ui.EntityUI;

import java.util.List;

@SpringUI(path = "smart/results")
@Theme("valo")
public class SmartServiceResultUI extends EntityUI<SmartServiceResult, Long> {

    private final SmartServiceResultRepository concreteRepository;

    public SmartServiceResultUI(SmartServiceResultRepository repository) {
        super(repository, SmartServiceResult.class);
        this.concreteRepository = repository;
    }

    @Override
    protected void init(VaadinRequest request) {
        super.init(request);
        grid.setColumns("error", "createdAt", "source", "serviceName", "result");
    }

    @Override
    protected List<SmartServiceResult> findEntityByFilter(String filter) {
        return concreteRepository.findByResultContainingOrServiceNameContainingAllIgnoringCaseOrderByCreatedAtDesc(filter, filter);
    }
}
