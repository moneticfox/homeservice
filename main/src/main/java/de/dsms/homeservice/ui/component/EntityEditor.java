package de.dsms.homeservice.ui.component;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import de.dsms.homeservice.entity.Identifiable;
import de.dsms.homeservice.ui.event.ChangeHandler;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.inject.Inject;
import java.io.Serializable;

public abstract class EntityEditor<T extends Identifiable<ID>, ID extends Serializable> extends VerticalLayout {

    protected final JpaRepository<T, ID> repository;
    protected final Class<T> entityClass;

    protected T entity;

    protected Button save = new Button("Speichern", FontAwesome.SAVE);
    protected Button cancel = new Button("Abbrechen");
    protected Button delete = new Button("Löschen", FontAwesome.TRASH_O);

    protected CssLayout actionStyles = new CssLayout(save, cancel, delete);

    protected HorizontalLayout actions;

    @Inject
    public EntityEditor(JpaRepository<T, ID> repository, Class<T> entityClass) {
        this.repository = repository;
        this.entityClass = entityClass;

        setSpacing(true);
        actionStyles.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        save.addClickListener(e -> repository.save(entity));
        delete.addClickListener(getDeleteClickListener());
        cancel.addClickListener(e -> setVisible(false));

        actions = new HorizontalLayout(save, delete, cancel);
        setVisible(false);
    }

    private Button.ClickListener getDeleteClickListener() {
        return (e) -> {
            if (isPersisted(entity)) {
                repository.delete(entity);
            }
        };
    }

    public final void editEntity(T t) {
        final boolean persisted = isPersisted(t);
        if (persisted) {
            entity = repository.findOne(t.getId());
        }
        else {
            entity = t;
        }
        delete.setVisible(persisted);

        BeanFieldGroup.bindFieldsUnbuffered(entity, this);

        setVisible(true);

        save.focus();
    }

    private boolean isPersisted(T t) {
        return t != null && t.getId() != null;
    }

    public void setChangeHandler(ChangeHandler h) {
        save.addClickListener(e -> h.onChange());
        delete.addClickListener(e -> h.onChange());
    }
}
