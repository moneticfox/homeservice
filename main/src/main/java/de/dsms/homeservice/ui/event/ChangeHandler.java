package de.dsms.homeservice.ui.event;

public interface ChangeHandler {

    void onChange();
}
