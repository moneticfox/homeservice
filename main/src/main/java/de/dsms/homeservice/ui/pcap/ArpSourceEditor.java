package de.dsms.homeservice.ui.pcap;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import de.dsms.homeservice.entity.ArpSource;
import de.dsms.homeservice.model.smart.SmartFunction;
import de.dsms.homeservice.repository.ArpSourceRepository;
import de.dsms.homeservice.ui.component.EntityEditor;

import javax.inject.Inject;

@SpringComponent
@UIScope
public class ArpSourceEditor extends EntityEditor<ArpSource, Long> {

    private TextField macAddress = new TextField("Mac-Adresse");
    private TextField name = new TextField("Gerätename");
    private ComboBox function = new ComboBox("Smart-Funktion") {{
        addItem(SmartFunction.SHOPPINGLIST);
    }};
    private TextField argument = new TextField("Argument");

    @Inject
    public ArpSourceEditor(ArpSourceRepository repository) {
        super(repository, ArpSource.class);
        addComponents(macAddress, name, function, argument, actions);
    }
}
