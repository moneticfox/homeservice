package de.dsms.homeservice.ui.pcap;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import de.dsms.homeservice.entity.ArpSource;
import de.dsms.homeservice.repository.ArpSourceRepository;
import de.dsms.homeservice.ui.EditableEntityUI;

import java.util.List;

@SpringUI(path = "pcap/sources")
@Theme("valo")
public class ArpSourceUI extends EditableEntityUI<ArpSource, Long> {

    private final ArpSourceRepository concreteRepository;

    public ArpSourceUI(ArpSourceRepository repository, ArpSourceEditor editor) {
        super(repository, editor, ArpSource.class);
        this.concreteRepository = repository;
    }

    @Override
    protected void init(VaadinRequest request) {
        super.init(request);
        grid.setColumns("name", "macAddress", "function", "argument", "createdAt");
    }

    @Override
    protected List<ArpSource> findEntityByFilter(String filter) {
        return concreteRepository.findByMacAddressContainingOrNameContainingAllIgnoringCaseOrderByCreatedAtDesc(filter, filter);
    }
}
